import './App.css';
import { useRef, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import HeaderHome from './common/headerHome/headerHome';
import HeaderUser from './common/headerUser/headerUser';

import Home   from './pages/home/home.tsx';
import About  from './pages/about/about.tsx';
import Login  from './pages/login/login.tsx';
import SignUp from './pages/signup/signup.tsx';

import Note    from './pages/note/note.tsx';
import Shop    from './pages/shop/shop.tsx';
import Cart    from './pages/cart/cart.tsx';
import Profile from './pages/profile/profile.tsx';

const pages = {
	home   : <Home />,
	about  : <About />,
	login  : <Login />,
	signup : <SignUp />,
	note   : <Note />,
	shop   : <Shop />,
	cart   : <Cart />,
	profile: <Profile />,
};
const unloggedPages = ['home', 'login','signup'];
const loginPages = ['note','shop','cart','profile'];

// check active session
function isLogged(){
	return 0;
}

function App(props) {

	const navigate = useNavigate();
	const header = useRef(null);
	const [bodyContent, setBodyContent] = useState();
	let {page} = props;

	useEffect(() => {
		console.log('debug: process');
		const logged = isLogged();

		// user logged
		if (logged){
			// send back to dashboard
			if (unloggedPages.includes(page)){
				page = 'note';
				navigate('/'+page);
			}

			header.current=<HeaderUser/>;
		}

		// user unlogged
		else{
			// send back to home
			if (loginPages.includes(page)){
				page = 'home';
				navigate('/');
			}

			header.current=<HeaderHome/>;
		}

		setBodyContent(pages[page]);
	}, []);

	return (
		<div className='App'>
			{header.current}
			{bodyContent}
		</div>
	);
};

export default App;
