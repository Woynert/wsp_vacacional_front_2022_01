import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

ReactDOM.createRoot(document.getElementById('root')!).render(
	<React.StrictMode>
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<App page='home' />} />
				<Route path='about' element={<App page='about' />} />
				<Route path='login' element={<App page='login' />} />
				<Route path='signup' element={<App page='signup' />} />

				<Route path='note' element={<App page='note' />} />
				<Route path='shop' element={<App page='shop' />} />
				<Route path='cart' element={<App page='cart' />} />
				<Route path='profile' element={<App page='profile' />} />

				<Route path='*' element={<p>404 Not Found</p>} />
			</Routes>
		</BrowserRouter>
	</React.StrictMode>
);
